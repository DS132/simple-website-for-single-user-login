<?php
// It is important to make sure that read permissions for this file are set correctly on your site!
// If this file is readable then a user can look up the username and password.

// Checks whether username and password entered match those expected.
if ($_POST['username']=="admin" && $_POST['password']=="pass")) {
	// Go to success page
    header('Location: ..\..\successPage.html');
}
else { //Not already logged in, not sent a password
	// Go to failure page
    header('Location: ..\..\failurePage.html');
}
?>